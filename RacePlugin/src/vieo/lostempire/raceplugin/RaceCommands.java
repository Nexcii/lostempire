package vieo.lostempire.raceplugin;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.entity.CraftWolf;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.NPC;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.topcat.npclib.NPCManager;
import com.topcat.npclib.entity.HumanNPC;

import vieo.lostempire.raceplugin.racenpcs.DwarfNPC;
import vieo.lostempire.raceplugin.racenpcs.ElfNPC;
import vieo.lostempire.raceplugin.racenpcs.ForsakenNPC;
import vieo.lostempire.raceplugin.racenpcs.RaceNPC;
import vieo.lostempire.skinplugin.SkinCore;

public class RaceCommands implements CommandExecutor
{
	private RaceCore plugin;
	private ArrayList<String> raceArray;
	
	public static Boolean DEBUG = true;
	
	public RaceCommands(RaceCore _plugin)
	{
		plugin = _plugin;
		
		raceArray = new ArrayList<String>();
		raceArray.add(Races.ELF);
		raceArray.add(Races.DWARF);
		raceArray.add(Races.VALKYRE);
		raceArray.add(Races.FORSAKEN);
	}	
	
	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String string, String[] args) 
	{
		String rootCommand = command.getName().toLowerCase();
				
		if(rootCommand.equals("race"))
		{
			return RaceMethod(commandSender, args);
		}
		
		return false;
	}
	
	private Boolean RaceMethod(CommandSender commandSender, String[] args)
	{
		if(args.length == 0)
		{
			commandSender.sendMessage("Your current race is: " + RaceHelper.getRace(commandSender.getName()));
			return true;
		}		
		
		
		if(args.length > 0)
		{
			if(args[0].equalsIgnoreCase("spawn"))
			{
				Player player = (Player)commandSender;
				
				if(args[1].equalsIgnoreCase("dwarf")) plugin.raceNPCManager.addRaceNPC(player.getLocation(), new DwarfNPC());
				if(args[1].equalsIgnoreCase("elf")) plugin.raceNPCManager.addRaceNPC(player.getLocation(), new ElfNPC());
				if(args[1].equalsIgnoreCase("forsaken")) plugin.raceNPCManager.addRaceNPC(player.getLocation(), new ForsakenNPC());
			}
			
			if(args[0].equalsIgnoreCase("test"))
			{
				SpoutPlayer player = (SpoutPlayer)commandSender;
				player.setSkin("http://127.0.0.1/render/Male/Dwarf/1/1/1/1/1/a.png");
			}
			
			if(args[0].equalsIgnoreCase("list"))
			{
				showRaceList(commandSender);		
			}
			else if(args[0].equalsIgnoreCase("help"))
			{
				commandSender.sendMessage("&4[Race Help]");
				commandSender.sendMessage("&f/race list");
				commandSender.sendMessage("&f/race info");
				commandSender.sendMessage("");
			}
			else if(args[0].equalsIgnoreCase("info"))
			{
				commandSender.sendMessage("&4[Race Info]");
				
				commandSender.sendMessage("&2Elf");
				commandSender.sendMessage("&2 + 20% Faster Movement ");
				commandSender.sendMessage("&2 + 30% More Damage With Bow");
				commandSender.sendMessage("&2 + x2 Damage On Shots Over 10 Blocks");
				commandSender.sendMessage("&2 + 10% Chance Of Poison Arrow");
				commandSender.sendMessage("&2  - 20% More Damage Taken");
				commandSender.sendMessage("");
				
				commandSender.sendMessage("&eDwarf");
				commandSender.sendMessage("&e + 20% Less Damage Taken");
				commandSender.sendMessage("&e + 40% More Damage With Axes");
				commandSender.sendMessage("&e + 5% Chance Of [Fast Digging]");
				commandSender.sendMessage("&e - Hungers Twice As Fast");
				commandSender.sendMessage("");
				
				commandSender.sendMessage("&9Forsaken");
				commandSender.sendMessage("&9 + 30% Chance Of [Slow] On Target");
				commandSender.sendMessage("&9 + 10% Chance Of [Life Steel] On Target");
				commandSender.sendMessage("&9 + 5% Chance Of [Blind] or [Confusion] On Target");
				commandSender.sendMessage("&9  + 20% Faster Movement");
				commandSender.sendMessage("&9 - 20% More Damage Taken");
				commandSender.sendMessage("");				
			}
		}
	
		return true;
	}
	
	private void showRaceList(CommandSender commandSender) 
	{
		commandSender.sendMessage("&4[Race List]");
		
		for (String raceString : raceArray)
		{
			commandSender.sendMessage(raceString);
		}
	}

}
