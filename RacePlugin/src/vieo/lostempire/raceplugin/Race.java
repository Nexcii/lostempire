package vieo.lostempire.raceplugin;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.Length;
import com.avaje.ebean.validation.NotEmpty;
import com.avaje.ebean.validation.NotNull;


@Entity
@Table(name="player_race")
public class Race 
{
	// [Boilerplate]
	@Id
	private int id;
	
	@NotNull
	private String playerName;
	
	@Length(max=30)
	@NotEmpty
	private String name;
	// [Boilerplate/]
	
	@NotEmpty
	private String race;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(playerName);
	}
	
	public void setPlayer(Player player)
	{
		this.playerName = player.getName();
	}
	
	// 

	public String getRace() {
		return race;
	}

	public void setRace(String test) {
		this.race = test;
	}
	
	
	
}
