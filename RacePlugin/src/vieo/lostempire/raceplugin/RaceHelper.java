package vieo.lostempire.raceplugin;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class RaceHelper
{	
	private static Random random;
	
	public static void setRace(Player player, String race)
	{
		String playerName = player.getName();
		Race raceData = RaceCore.instance.getDatabase().find(Race.class).where().ieq("name", playerName).findUnique();
		
		// If data dosn't exist
		if(raceData == null)
		{
			raceData = new Race();
			raceData.setName(playerName);
			raceData.setPlayerName(playerName);
		}
		
		raceData.setRace(race);
		
		RaceChangeEvent event = new RaceChangeEvent(player, race);
		// Call the event
		Bukkit.getServer().getPluginManager().callEvent(event);
		
		RaceCore.instance.getDatabase().save(raceData);
	}	
	
	public static String getRace(String playerName)
	{
		Race raceData = RaceCore.instance.getDatabase().find(Race.class).where().ieq("name", playerName).findUnique();
		
		return (raceData != null) ? raceData.getRace() : Races.NOVICE;
	}	
	
	public static int random(int min, int max)
	{
		if(random == null) random = new Random();
		
		return random.nextInt((max+1) - min) + min;
	}
}
