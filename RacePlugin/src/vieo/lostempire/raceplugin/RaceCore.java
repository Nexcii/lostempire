package vieo.lostempire.raceplugin;

import java.util.List;

import javax.persistence.PersistenceException;

import net.minecraft.server.Item;
import net.minecraft.server.World;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.getspout.spoutapi.material.Weapon;

import com.topcat.npclib.NPCManager;
import com.topcat.npclib.entity.HumanNPC;
import com.topcat.npclib.nms.NpcEntityTargetEvent;
import com.topcat.npclib.nms.NpcEntityTargetEvent.NpcTargetReason;

import vieo.lostempire.raceplugin.racelisteners.DwarfListener;
import vieo.lostempire.raceplugin.racelisteners.ElfListener;
import vieo.lostempire.raceplugin.racelisteners.ForsakenListener;
import vieo.lostempire.raceplugin.racenpcs.RaceNPC;
import vieo.lostempire.raceplugin.racenpcs.RaceNPCManager;


public class RaceCore extends JavaPlugin  implements Listener
{
	public static RaceCore instance;
	
	public NPCManager npcManager;
	public RaceNPCManager raceNPCManager;
	
	public void onEnable()
	{ 
		instance = this;
		
		getLogger().info("Race plugin started. Starting DB..");
		
		getCommand("race").setExecutor(new RaceCommands(this));
		getServer().getPluginManager().registerEvents(this, this);
		
		getServer().getPluginManager().registerEvents(new DwarfListener(), this);
		getServer().getPluginManager().registerEvents(new ElfListener(), this);
		getServer().getPluginManager().registerEvents(new ForsakenListener(), this);
		
		npcManager = new NPCManager(this);
		raceNPCManager = new RaceNPCManager(npcManager);
		
		getServer().getPluginManager().registerEvents(raceNPCManager, this);
		
		setupDatabase();		
	}
	 
	public void onDisable()
	{ 
	 
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event)
	{   	
		Player player = event.getPlayer();	
	}
	
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{
	    if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent ee = (EntityDamageByEntityEvent)event;
	        
	       
	    }
	}
	

	private void setupDatabase()
	{
		try
		{
			getDatabase().find(Race.class).findRowCount();
		}
		catch (PersistenceException e)
		{
			getLogger().warning(e.toString());
			installDDL();
		}
	}
	
	@Override
	public List<Class<?>> getDatabaseClasses() {
		// TODO Auto-generated method stub
		List<Class<?>> classes = super.getDatabaseClasses();
		classes.add(Race.class);
		
		return classes;
	}
	
	
	
}

