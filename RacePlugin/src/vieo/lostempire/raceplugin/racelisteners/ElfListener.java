package vieo.lostempire.raceplugin.racelisteners;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.getspout.spoutapi.event.spout.SpoutCraftEnableEvent;

import vieo.lostempire.raceplugin.RaceHelper;
import vieo.lostempire.raceplugin.RaceListener;
import vieo.lostempire.raceplugin.Races;

public class ElfListener extends RaceListener
{
	public ElfListener() { super(Races.ELF); }	
	
	@Override public double getDamageTakenMultiplier() { return 1.2; }
	@Override public double getWalkingSpeedMultiplier() { return 1.2; }
	
	@Override
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{	
		super.onEntityDamage(event);
		
		if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent)event;
	        
	        if(entityEvent.getDamager() instanceof Arrow)
	        {
	            Entity shooter = ((Projectile) entityEvent.getDamager()).getShooter();
	            
	            if(shooter instanceof Player)
	            {
	            	Player player = (Player)shooter;
	            		            	
	            	if(RaceHelper.getRace(player.getName()).equalsIgnoreCase(Races.ELF))
	            	{
	            		int originalDamage = event.getDamage();
		            	int newDamage = (int)(originalDamage + (originalDamage * 0.3));
		            	
		            	
		            	int distance = Distance2D(player.getLocation().getBlockX(), player.getLocation().getBlockZ(), event.getEntity().getLocation().getBlockX(), event.getEntity().getLocation().getBlockZ());
		            	
		            	//player.sendMessage("Distance:" + distance);
		            	
		            	if(distance > 10)
		            	{
		            		player.sendMessage("[Long Shot] x2 Damage!");
		            		newDamage *= 2;
		            	}
		            	
		            	event.setDamage(newDamage);	
		            	player.sendMessage("Elf Bonus +" + (newDamage - originalDamage) + "!");		            			
		            			
		            	if(RaceHelper.random(0, 100) < 10)
		            	{
		            		((LivingEntity)entityEvent.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 500, 100));
		            	}		            	
	            	}
	            }
	        }	        
	    }
	}
	
	 public int Distance2D(int x1, int y1, int x2, int y2)
     {
         int result = 0;
         
         double part1 = Math.pow((x2 - x1), 2);
         double part2 = Math.pow((y2 - y1), 2);
         double underRadical = part1 + part2;

         result = (int)Math.sqrt(underRadical);
         return Math.abs(result);
     }

}
