package vieo.lostempire.raceplugin.racelisteners;

import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.getspout.spoutapi.player.SpoutPlayer;

import vieo.lostempire.raceplugin.RaceHelper;
import vieo.lostempire.raceplugin.RaceListener;
import vieo.lostempire.raceplugin.Races;

public class DwarfListener extends RaceListener
{
	public DwarfListener() { super(Races.DWARF); }
	
	@Override public double getDamageTakenMultiplier() { return 0.8; }
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{		
	    if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent)event;
	        
	        if(entityEvent.getDamager() instanceof Player)
	        {
	        	Player player = ((Player)entityEvent.getDamager());
	        	
	        	if(RaceHelper.getRace(player.getName()).equalsIgnoreCase(Races.DWARF))
	        	{
	        		
		        	if(entityEvent.getCause() == DamageCause.ENTITY_ATTACK)
		        	{
		        		if(player.getItemInHand().getType() == Material.WOOD_AXE ||
		        		   player.getItemInHand().getType() == Material.STONE_AXE ||
		        		   player.getItemInHand().getType() == Material.IRON_AXE ||
		        		   player.getItemInHand().getType() == Material.GOLD_AXE ||
		        		   player.getItemInHand().getType() == Material.DIAMOND_AXE)
		        		{
		        			int originalDamage = event.getDamage();
			            	int newDamage = (int)(originalDamage + (originalDamage * 0.4));
			            	
			            	event.setDamage(newDamage);
			            	player.sendMessage("Dwarf Bonus +" + (newDamage - originalDamage) + "!");
		        		}
		        	}
	        	}
	        	
	        }
	    }
	}
	
	@EventHandler
	public void onPlayerUse(PlayerInteractEvent event)
	{
	    Player p = event.getPlayer();
	    
	    if(p.getItemInHand().getType() == Material.BLAZE_POWDER)
	    {
	    	
	    }
	    
	    /*
	    if(p.getItemInHand().getType() == Material.BLAZE_POWDER)
	    {
	        Fireball fire = p.getWorld().spawn(event.getPlayer().getLocation().add(new Vector(0.0D, 1.0D, 0.0D)), Fireball.class);
	        fire.setShooter(p);
	    }
	    */
	}
	
	/*
	@EventHandler
    public void foodLevelChange(FoodLevelChangeEvent event)
	{
        if (event.isCancelled())
        {
            return;
        }
        if (event.getEntity() instanceof Player)
        {
            Player p = (Player) event.getEntity();
            
            // If the world has hunger set to false, do not let the level go down
            p.sendMessage("Hungrry!");
            event.setFoodLevel(((Player)event.getEntity()).getFoodLevel() - 4);
            event.setCancelled(false);
        }
    }
	*/
	
	@EventHandler
    public void onBlockBreak(BlockBreakEvent event)
	{
        Player player = event.getPlayer();
        SpoutPlayer spoutPlayer = (SpoutPlayer)player;
        
        
        if(RaceHelper.getRace(player.getName()).equalsIgnoreCase(Races.DWARF))
        {
	        if(RaceHelper.random(0, 100) < 5)
	        {
	        	player.sendMessage("Dwarf Bonus: Beserk!");
	        	player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 20 * 5, 2));        
	        }
        }
    }

}
