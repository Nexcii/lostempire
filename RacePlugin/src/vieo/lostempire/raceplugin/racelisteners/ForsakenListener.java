package vieo.lostempire.raceplugin.racelisteners;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import vieo.lostempire.raceplugin.RaceHelper;
import vieo.lostempire.raceplugin.RaceListener;
import vieo.lostempire.raceplugin.Races;

public class ForsakenListener extends RaceListener
{
	public ForsakenListener() { super(Races.FORSAKEN); }
	
	@Override public double getWalkingSpeedMultiplier() { return 1.2; }
	@Override public double getDamageTakenMultiplier() { return 1.2; }
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{		
	    if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent)event;
	        
	        if(entityEvent.getDamager() instanceof Player)
	        {
	        	Player player = ((Player)entityEvent.getDamager());
	        	LivingEntity target = (LivingEntity)entityEvent.getEntity();
	        			
	        	if(RaceHelper.getRace(player.getName()).equalsIgnoreCase(Races.FORSAKEN))
	        	{
	        		if(RaceHelper.random(0, 100) < 30)
	        		{       			
	        			target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1, 1));
	        			
	        			player.sendMessage("Forsaken Bonus: Slow!");
	        		}
	        		
	        		if(RaceHelper.random(0, 100) < 10)
	        		{
	        			player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 1, 1));	        			
	        			target.addPotionEffect(new PotionEffect(PotionEffectType.HARM, 1, 1));
	        			
	        			player.sendMessage("Forsaken Bonus: Life Steal");
	        		}
	        		
	        		if(RaceHelper.random(0, 100) < 5)
	        		{
	        			target.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1, 1));	        			
	        			player.sendMessage("Forsaken Bonus: Blind");
	        		}
	        		
	        		if(RaceHelper.random(0, 100) < 5)
	        		{
	        			target.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1, 1));	        			
	        			player.sendMessage("Forsaken Bonus: Confustion");
	        		}
	        	}
	        	
	        }
	    }
	}
}
