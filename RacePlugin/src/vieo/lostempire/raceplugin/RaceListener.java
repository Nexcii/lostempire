package vieo.lostempire.raceplugin;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.getspout.spoutapi.event.spout.SpoutCraftEnableEvent;
import org.getspout.spoutapi.player.SpoutPlayer;

public class RaceListener implements Listener
{	
	private String _raceName = Races.NOVICE;
	
	public RaceListener(String raceName)
	{
		_raceName = raceName;
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{	
		if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent)event;      
	        
            if(entityEvent.getEntity() instanceof Player)
            {
            	Player player = (Player)entityEvent.getEntity();
            	
            	if(RaceHelper.getRace(player.getName()).equalsIgnoreCase(_raceName))
            	{
            		int originalDamage = event.getDamage();
            		double newDamage = originalDamage * getDamageTakenMultiplier();
            		int newDamageRounded = (int)Math.round(newDamage);
            		
            		//player.sendMessage("Original: " + originalDamage + " New: " + newDamageRounded + " Multiplier: " + getDamageMultiplier());
            		
            		event.setDamage(newDamageRounded);
            	}
            }
	    }
	}

	@EventHandler
	public void OnRaceChange(RaceChangeEvent event)
	{
		if(RaceHelper.getRace(event.getPlayer().getName()).equalsIgnoreCase(_raceName))
		{
			OnEnable((SpoutPlayer)event.getPlayer());
		}
	}
	
	public void OnEnable(SpoutPlayer player)
	{
		player.setWalkingMultiplier(getWalkingSpeedMultiplier());
		player.sendMessage("Speed: " + getWalkingSpeedMultiplier());
	}
	
	@EventHandler
	public void onSpoutEnabled(SpoutCraftEnableEvent event)
	{		
		if(RaceHelper.getRace(event.getPlayer().getName()).equalsIgnoreCase(_raceName))
		{
			OnEnable(event.getPlayer());
		}
	}
	
	public double getDamageTakenMultiplier()
	{
		return 1;
	}
	
	public double getWalkingSpeedMultiplier()
	{
		return 1;
	}
}
