package vieo.lostempire.raceplugin;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
 
public class RaceChangeEvent extends Event
{
	
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private String race;
 
    public RaceChangeEvent(Player _player, String _race)
    {
    	player = _player;
    	race = _race;
    }
 
    public Player getPlayer()
    {
        return player;
    }
    
    public String getRace()
    {
        return race;
    }
 
    public HandlerList getHandlers()
    {
        return handlers;
    }
 
    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}