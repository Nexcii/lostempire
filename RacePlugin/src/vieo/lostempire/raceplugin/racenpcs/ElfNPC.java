package vieo.lostempire.raceplugin.racenpcs;

import vieo.lostempire.raceplugin.Races;

public class ElfNPC extends RaceNPC
{
	@Override
	public String getName()	{ return "Elf Master"; } 
	
	@Override
	public String getSkinURL() { return "https://dl.dropbox.com/s/r0pzoteeo7j1peo/elf.png"; }
	
	@Override
	public String getRace() { return Races.ELF; }
}
