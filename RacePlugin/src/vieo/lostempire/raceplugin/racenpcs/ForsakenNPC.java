package vieo.lostempire.raceplugin.racenpcs;

import vieo.lostempire.raceplugin.Races;

public class ForsakenNPC extends RaceNPC
{
	@Override
	public String getName()	{ return "Forsaken Master"; } 
	
	@Override
	public String getSkinURL() { return "https://dl.dropbox.com/s/a0l6n3f1afsv19r/forsaken.png"; }
	
	@Override
	public String getRace() { return Races.FORSAKEN; }
}
