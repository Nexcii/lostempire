package vieo.lostempire.raceplugin.racenpcs;

import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import vieo.lostempire.raceplugin.RaceAcceptView;
import vieo.lostempire.raceplugin.RaceCommands;
import vieo.lostempire.raceplugin.RaceHelper;
import vieo.lostempire.raceplugin.Races;

import com.topcat.npclib.entity.HumanNPC;
import com.topcat.npclib.nms.NpcEntityTargetEvent.NpcTargetReason;

public class RaceNPC 
{	
	private HumanNPC entity;
	
	public void Spanwed(HumanNPC npcEntity)
	{
		entity = npcEntity;
		
		npcEntity.getSpoutPlayer().setSkin(getSkinURL());
	}
	
	public void PlayerInterace(Player p, NpcTargetReason npcTargetReason)
	{
		if(npcTargetReason == NpcTargetReason.NPC_RIGHTCLICKED)
		{
			StartDialog(p);			
		}			
	}
	
	public void StartDialog(Player p)
	{
		SpoutPlayer spoutPlayer = (SpoutPlayer)p;		
		
		if(RaceHelper.getRace(spoutPlayer.getName()) == Races.NOVICE || RaceCommands.DEBUG)
		{
			
			spoutPlayer.getMainScreen().attachPopupScreen(new RaceAcceptView(getRace()));
		}
		else
		{
			Chat(spoutPlayer, "You've already choosen your class!");
		}
	}
	
	public void Chat(Player player, String chat)
	{
		player.sendMessage("<" + entity.getName() + "> " + chat);
	}
	
	public String getSkinURL()
	{
		return "http://127.0.0.1/skin/Male/Dwarf/1/1/1/1/1/a.png";
	}
	
	public String getRace()
	{
		return "Abstract opening Line";
	}
	
	public String getName()
	{
		return "Abstract Race NPC";
	}
}
