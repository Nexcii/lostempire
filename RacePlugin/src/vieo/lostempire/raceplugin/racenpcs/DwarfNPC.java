package vieo.lostempire.raceplugin.racenpcs;

import vieo.lostempire.raceplugin.Races;

public class DwarfNPC extends RaceNPC
{	
	@Override
	public String getName()	{ return "Dwarf Master"; }
	
	@Override
	public String getSkinURL() { return "https://dl.dropbox.com/s/9i1v8bi9whtu4jv/dwarf.png"; }

	@Override
	public String getRace() { return Races.DWARF; }
}
