package vieo.lostempire.raceplugin.racenpcs;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.getspout.spoutapi.event.spout.SpoutCraftEnableEvent;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.topcat.npclib.NPCManager;
import com.topcat.npclib.entity.HumanNPC;
import com.topcat.npclib.nms.NpcEntityTargetEvent;

public class RaceNPCManager implements Listener
{
	private NPCManager npcManager; 
	
	private HashMap<String, HumanNPC> raceNPCs = new HashMap<String, HumanNPC>();
	private HashMap<String, RaceNPC> raceNPCTypes = new HashMap<String, RaceNPC>();
	
	public RaceNPCManager(NPCManager _npcManager)
	{
		npcManager = _npcManager;
	}
	
	public void addRaceNPC(Location location, RaceNPC raceNPC)
	{
		HumanNPC spawnedNPC = (HumanNPC)npcManager.spawnHumanNPC(raceNPC.getName(), location);
		
		raceNPCs.put(raceNPC.getName(), spawnedNPC);
		raceNPCTypes.put(raceNPC.getName(), raceNPC);
		
		raceNPC.Spanwed(spawnedNPC);
	}
	
	
	@EventHandler
	public void onSpoutLogin(SpoutCraftEnableEvent event)
	{   	
		SpoutPlayer player = (SpoutPlayer)event.getPlayer();
		
		for (HumanNPC npc : raceNPCs.values())
		{
			String skinURL = raceNPCTypes.get(npc.getName()).getSkinURL();
			npc.getSpoutPlayer().setSkin(skinURL);
		}
	}
	
	@EventHandler
    public void onEntityTarget(EntityTargetEvent event) 
	{
        if (event instanceof NpcEntityTargetEvent)
        {
            NpcEntityTargetEvent nevent = (NpcEntityTargetEvent)event;
            
            HumanNPC humanNPC = (HumanNPC)npcManager.getNPC(npcManager.getNPCIdFromEntity(event.getEntity()));
            
            if(humanNPC != null && event.getTarget() instanceof Player)
            {
            	if(raceNPCs.containsKey(humanNPC.getName()));
            	{
            		RaceNPC raceNPC = raceNPCTypes.get(humanNPC.getName());
	            	Player p = (Player) event.getTarget();
	            	raceNPC.PlayerInterace(p, nevent.getNpcReason());
            	}
            }     
            
        }

    }
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{	
		if (event instanceof EntityDamageByEntityEvent)
	    {
	        EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent)event;      

	        HumanNPC humanNPC = (HumanNPC)npcManager.getNPC(npcManager.getNPCIdFromEntity(event.getEntity()));

            if(humanNPC != null)
            {
            	event.setDamage(0);
            	entityEvent.setCancelled(false);
            }
	    }
	}
}
