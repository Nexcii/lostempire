package vieo.lostempire.raceplugin;

public class Races
{
	public static String NOVICE = "NOVICE";
	
	public static String ELF = "ELF";
	public static String FORSAKEN = "FORSAKEN";
	public static String DWARF = "DWARF";
	public static String VALKYRE = "VALKYRE";
}
