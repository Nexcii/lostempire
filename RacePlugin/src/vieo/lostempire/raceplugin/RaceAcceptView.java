package vieo.lostempire.raceplugin;

import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.gui.ContainerType;
import org.getspout.spoutapi.gui.GenericButton;
import org.getspout.spoutapi.gui.GenericContainer;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.GenericTexture;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.WidgetAnchor;

public class RaceAcceptView extends GenericPopup
{
	private final String race;
	
	private GenericLabel infoLabel;
	
	
	
	private GenericContainer container;
	
	private GenericButton acceptButton;
	private GenericButton rejectButton;
	
	public RaceAcceptView(String _race)
	{
		super();		
		
		race = _race;
		
		
		
		container = new GenericContainer();
		
		acceptButton = new GenericButton("Accept")
		{
			@Override
			public void onButtonClick(ButtonClickEvent event)
			{
				event.getPlayer().sendMessage("&4[Race] Congratulations you are now a " + race + ".");
				RaceHelper.setRace(event.getPlayer(), race);
				
				close();
			}
		};
		
		rejectButton = new GenericButton("Reject")
		{
			@Override
			public void onButtonClick(ButtonClickEvent event)
			{
				close();
			}
		};		
		
		container.setWidth(200);
		
		acceptButton.setMaxHeight(20);
		acceptButton.setWidth(100);
		rejectButton.setMaxHeight(20);
		rejectButton.setWidth(100);
		
		container.setAlign(WidgetAnchor.CENTER_CENTER);
		container.setAnchor(WidgetAnchor.CENTER_CENTER);
		container.setLayout(ContainerType.HORIZONTAL);
		
		container.setX(-container.getWidth() / 2);
		
		infoLabel = new GenericLabel("Do you wish to become a " + race + "?");
		infoLabel.setAnchor(WidgetAnchor.CENTER_CENTER);
		infoLabel.setAlign(WidgetAnchor.CENTER_CENTER);
		infoLabel.setX(-infoLabel.getWidth() / 2);
		
		GenericTexture texture = new GenericTexture("https://dl.dropbox.com/s/nl5d0hfsqb2jar8/backdrop.png");
		texture.setWidth(844);
		texture.setHeight(358);
		
		texture.setAnchor(WidgetAnchor.CENTER_CENTER);
		texture.setX(-texture.getWidth() / 2);
		texture.setY(-texture.getHeight() / 2);
		
		
				
		acceptButton.setPriority(RenderPriority.Highest);
		rejectButton.setPriority(RenderPriority.Highest);
		
		container.addChildren(acceptButton, rejectButton);
		

		attachWidget(getPlugin(), texture);
		attachWidget(getPlugin(), infoLabel);
		attachWidget(getPlugin(), container);
	}
	
	
}
