<?php

	$race = $_GET["race"];
	$gender = $_GET["gender"];

	$genderRaceDir = "Assets/" . $race . "/" . $gender . "/";

	printAssetData($genderRaceDir, "skin");
	printAssetData($genderRaceDir, "bottom");
	printAssetData($genderRaceDir, "top");
	printAssetData($genderRaceDir, "hair");
	printAssetData($genderRaceDir, "face");
	
	function printAssetData($dir, $catagory)
	{
		$count = getAssetCount($dir . $catagory);	
		echo $catagory . ":" . $count . "|";
	}

	function getAssetCount($url)
	{
		if (glob($url . "/" . "*.png") != false)
		{
			return count(glob($url . "/" . "*.png"));
		}
		else
		{
			return 0;
		}
	}
?>
