<?php
	// http://127.0.0.1/skin.php?gender=Male&race=Dwarf&skin=1&bottom=1&top=1&hair=2&face=1
	
	// Fetch all indexs
	$race = $_GET["race"];
	$gender = $_GET["gender"];

	$skinIndex = $_GET["skin"];
	$bottomIndex = $_GET["bottom"];
	$topIndex = $_GET["top"];
	$hairIndex = $_GET["hair"];
	$faceIndex = $_GET["face"];

	$assetURL = "Assets/" . $race . "/" . $gender . "/";

	// Create blank image
	$im = imagecreatefrompng($assetURL . "Skin/" . $skinIndex . ".png");
	
	imagealphablending($im, true);
	imagesavealpha($im, true);
	
	// Setup layers
	imagecopy ($im, imagecreatefrompng($assetURL . "Bottom/" . $bottomIndex . ".png"), 0, 0, 0, 0, 64, 32);
	imagecopy ($im, imagecreatefrompng($assetURL . "Top/" . $topIndex . ".png"), 0, 0, 0, 0, 64, 32);
	imagecopy ($im, imagecreatefrompng($assetURL . "Hair/" . $hairIndex . ".png"), 0, 0, 0, 0, 64, 32);
	imagecopy ($im, imagecreatefrompng($assetURL . "Face/" . $faceIndex . ".png"), 0, 0, 0, 0, 64, 32);
	
	// Output
	header('Content-type: image/png');
	
	imagepng($im);
	imagedestroy($im);
	
?>