package vieo.lostempire.skinplugin.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class SkinUtils
{
	private static Random random;
	
	public static int random(int min, int max)
	{
		if(random == null) random = new Random();
		
		return random.nextInt((max+1) - min) + min;
	}
	
	public static String downloadURL(String _url)
	{
		URL url;
		InputStream is = null;
		DataInputStream dis;
		String line;
		String builder = "";
		
		try
		{
		    url = new URL(_url);
		    is = url.openStream();  // throws an IOException
		    dis = new DataInputStream(new BufferedInputStream(is));		    
		    
		    while ((line = dis.readLine()) != null)
		    {
		    	builder += line;
		    }
		} 
		catch (MalformedURLException mue)
		{
		     mue.printStackTrace();
		}
		catch (IOException ioe)
		{
		     ioe.printStackTrace();
		}
		finally
		{
		    try
		    {
		        is.close();
		    }
		    catch (IOException ioe)
		    {
		    }
		}
		
		return builder;
	}
	
}
