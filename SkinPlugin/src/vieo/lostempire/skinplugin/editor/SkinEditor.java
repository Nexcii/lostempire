package vieo.lostempire.skinplugin.editor;

import java.util.logging.Level;

import org.bukkit.plugin.Plugin;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.GenericTexture;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.WidgetAnchor;

import vieo.lostempire.skinplugin.editor.buttons.ChangeSkinAssetButton;
import vieo.lostempire.skinplugin.utils.SkinUtils;

public class SkinEditor extends GenericPopup
{
	public AvatarSkin avatarSkin;
	public Plugin plugin;
	
	GenericTexture preview;
	
	ChangeSkinAssetButton randomButton;
	
	public SkinEditor(Plugin _plugin)
	{
		super();		
		plugin = _plugin;		
		
		avatarSkin = AvatarSkin.getDefaultSkin();	
		
		getPlugin().getLogger().log(Level.INFO, "Making skin editor");
		
		setupPreview();
		setupButtons(); 
		
		updateSkin();
	}
	
	private void setupPreview()
	{
		preview = new GenericTexture();
		
        preview.setWidth(240 / 3);
        preview.setHeight(480 / 3);
        preview.setMaxWidth(240 / 3);
        
        preview.setX(-((240 / 3) / 2));
        preview.setY(-((480 / 3) / 2));
        preview.setAnchor(WidgetAnchor.CENTER_CENTER);
        
		preview.setUrl(avatarSkin.getURL());
        
		getPlugin().getLogger().log(Level.INFO, "Pew URL: " + avatarSkin.getURL());
		
        attachWidget(plugin, preview);   
	}
	
	private void setupButtons()
	{
		randomButton = new ChangeSkinAssetButton(this)
	     {
			@Override
	       	public void changeAsset(AvatarSkin avatarSkin)
	       	{
	       		avatarSkin.skinID = SkinUtils.random(1, 6);
	       		avatarSkin.bottomsID = SkinUtils.random(1, 3);
	       		avatarSkin.topID = SkinUtils.random(1, 5);
	       		avatarSkin.faceID = SkinUtils.random(1, 2);
	       		avatarSkin.hairID = SkinUtils.random(1, 10);	
	       		
	       		super.changeAsset(avatarSkin);
	       	}
	       };        
	       randomButton.setText("Random");
	       
	       
       
       
        
        attachWidget(plugin, randomButton);   
	}

	public void updateSkin()
	{
		// Log it
		getPlugin().getLogger().log(Level.INFO, "Skin URL: " + avatarSkin.getURL());
		
		preview.setUrl(avatarSkin.getURL());		
	}	
	
}
