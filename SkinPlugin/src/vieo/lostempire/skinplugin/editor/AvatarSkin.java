package vieo.lostempire.skinplugin.editor;

import java.util.Random;

import vieo.lostempire.skinplugin.utils.SkinUtils;

public class AvatarSkin
{

	public static AvatarSkin getDefaultSkin()
	{
		AvatarSkin defautlAvatarSkin = new AvatarSkin();
		defautlAvatarSkin.skinID = 1;
		defautlAvatarSkin.bottomsID = 1;
		defautlAvatarSkin.topID = 1;
		defautlAvatarSkin.faceID = 1;
		defautlAvatarSkin.hairID = 1;		
		
		return defautlAvatarSkin;
	}
	
	public static AvatarSkin getRandomSkin()
	{		
		AvatarSkin randomAvatarSkin = new AvatarSkin();
		
		randomAvatarSkin.skinID = SkinUtils.random(1, 6);
		randomAvatarSkin.bottomsID = SkinUtils.random(1, 3);
		randomAvatarSkin.topID = SkinUtils.random(1, 5);
		randomAvatarSkin.faceID = SkinUtils.random(1, 2);
		randomAvatarSkin.hairID = SkinUtils.random(1, 10);		
		
		return randomAvatarSkin;
	} 
	
	public String getURL()
	{
		Random random = new Random();
		String hostURL = "http://127.0.0.1";
		
		
		
		return hostURL + "/render/" + gender + "/" + race + "/" + skinID + "/" + bottomsID + "/" + topID + "/" + hairID + "/" + faceID + "/" +  generateCacheTag() + ".png";
	}
	
	private String generateCacheTag()
	{
		return skinID + "-" + bottomsID + "-" + topID + "-" + hairID + "-" + faceID;
	}
	
	public int skinID = 1;
	public int bottomsID = 1;
	public int topID = 1;
	public int faceID = 1;
	public int hairID = 1;
	public String gender = "Male";
	public String race = "Dwarf";
}
