package vieo.lostempire.skinplugin.editor.buttons;

import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.gui.GenericButton;
import org.getspout.spoutapi.gui.WidgetAnchor;

import vieo.lostempire.skinplugin.editor.AvatarSkin;
import vieo.lostempire.skinplugin.editor.SkinEditor;

public class ChangeSkinAssetButton extends GenericButton
{
	private SkinEditor editor;
	
	public ChangeSkinAssetButton(SkinEditor _editor)
	{
		super();
		
		editor = _editor;
		
		setHeight(20);
		setWidth(100);
		setAnchor(WidgetAnchor.BOTTOM_CENTER);
		setX(-getWidth() / 2);
		setY(-(getHeight() + 15));
	}
	
	@Override
	public void onButtonClick(ButtonClickEvent event)
	{
		changeAsset(editor.avatarSkin);	
		
		super.onButtonClick(event);
	}
	
	// Used for override
	public void changeAsset(AvatarSkin avatarSkin)
	{
		editor.updateSkin();
	}
	
}
